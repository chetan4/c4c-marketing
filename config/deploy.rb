set :application, 'c4c-marketing'
set :repo_url, 'git@github.com:idyllicsoftware/c4c-marketing.git'
set :branch, 'master'
set :scm, :git
set :format, :pretty
set :log_level, :debug
set :deploy_to, '/srv/apps/c4c-marketing'
# set :pty, true

set :linked_files, %w{config/database.yml}
set :linked_dirs, %w{log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system}


set :rvm1_type, :user
set :rvm1_ruby_version, 'ruby-2.1.0@c4c-marketing'
                                                                # set :default_env, { path: "/opt/ruby/bin:$PATH" }
set :keep_releases, 5
set :bundle_roles, :all                                  # this is default
set :bundle_servers, -> { release_roles(fetch(:bundle_roles)) } # this is default
set :bundle_gemfile, -> { release_path.join('Gemfile') }
                                                                #set :bundle_dir, -> { shared_path.join('bundle') }
set :bundle_flags, '--deployment'
set :bundle_without, %w{development test}.join(' ')
                                                                #set :bundle_binstubs, -> { shared_path.join('bin') }
set :bundle_roles, :all
                                                                #set :normalize_asset_timestamps, %{public/images public/javascripts
                                                                # public/stylesheets}

SSHKit.config.command_map[:rake]  = "bundle exec rake"
SSHKit.config.command_map[:rails]  = "bundle exec rails"



# ask :branch, proc { `git rev-parse --abbrev-ref HEAD`.chomp }

# set :deploy_to, '/var/www/my_app'
# set :scm, :git

# set :format, :pretty
# set :log_level, :debug
# set :pty, true

# set :linked_files, %w{config/database.yml}
# set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system}

# set :default_env, { path: "/opt/ruby/bin:$PATH" }
# set :keep_releases, 5

namespace :deploy do

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      # Your restart mechanism here, for example:
        execute :touch, release_path.join('tmp/restart.txt')
    end
  end

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      # within release_path do
      #   execute :rake, 'cache:clear'
      # end
    end
  end

  after :finishing, 'deploy:cleanup'

end
