class AddUniqueConstraintToEmailInEenquiries < ActiveRecord::Migration
  def change
    add_index :enquiries, :email, :unique => true
  end
end
