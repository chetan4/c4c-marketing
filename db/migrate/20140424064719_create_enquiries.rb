class CreateEnquiries < ActiveRecord::Migration
  def change
    create_table :enquiries do |t|
      t.string :name
      t.string :phone
      t.string :email, :null => false, :unqiue => true
      t.string :blog_url

      t.timestamps
    end
  end
end
