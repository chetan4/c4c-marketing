class Notifier < ActionMailer::Base
  default from: "notifier@welcome.c4chq.com"

  def new_enquiry_mail(enquiry)
    @enquiry = enquiry
    mail(to: ADMIN_PRIMARY, :cc => ADMIN, from: 'notifications@welcome.c4chq.com', subject: 'New Enquiry on the marketing page.')
  end
end
