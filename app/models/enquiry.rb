# == Schema Information
#
# Table name: enquiries
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  phone      :string(255)
#  email      :string(255)
#  blog_url   :string(255)
#  created_at :datetime
#  updated_at :datetime
#

class Enquiry < ActiveRecord::Base
  validates_presence_of :email, :name, :blog_url
  validates_uniqueness_of :email
  validates_format_of :email, :with => /\A[^@]+@([^@\.]+\.)+[^@\.]+\z/

  validate :blog_url_validation

  def blog_url_validation
    begin
      uri = URI.parse(self.blog_url)
      self.errors.add(:blog_url, 'Invalid Blog Url') unless ((uri.kind_of?(URI::HTTP)) || (uri.kind_of?(URI::HTTPS)))
    rescue => e
      Rails.logger.debug("#{e.message} \n #{e.backtrace}")
      self.errors.add(:blog_url, 'Invalid Blog Url')
    end
  end
end
