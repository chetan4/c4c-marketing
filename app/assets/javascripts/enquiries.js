// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.
var C4CMarketing = C4CMarketing || {}

 C4CMarketing = (function($, window, document, CM){
     CM.Enquiry = {
         newEnquiry: function(){
           $('#get-started').delegate('#sign-up-form', 'submit', function(e){
               e.preventDefault();

               var ajaxOptions = {
                   url: $(this).attr('action'),
                   type: 'POST',
                   data: $(this).serialize(),
                   success: function(response){
                        if(response.request_status == 'success'){
                            $('#get-started').hide();
                            $('#thank-you').show();
                        }else{
                            $('#get-started').hide();
                            $('#awkward-moment').show();
                        }
                   },
                   error: function(response){
                     $('#get-started').hide();
                     $('#awkward-moment').show();
                   }
               };


               $.ajax(ajaxOptions);
           });
         }
     }


     $(function(){
        if ($('#sign-up-form').length > 0)
            CM.Enquiry.newEnquiry();
     })
     return CM;
 })(jQuery, this, this.document, C4CMarketing);