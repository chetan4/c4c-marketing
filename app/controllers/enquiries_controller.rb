class EnquiriesController < ApplicationController
  def create
    @enquiry = Enquiry.new(:name => params[:FirstName],
                           :email => params[:Email],
                           :blog_url => params[:BlogUrl],
                           :phone => params[:phone]

                          )

    if Enquiry.where(:email => @enquiry.email).exists?
      render :json => {:request_status => :success}, :status => :ok
    else
      if @enquiry.save
        Notifier.new_enquiry_mail(@enquiry).deliver
        render :json => {:request_status => :success}, :status => :ok
      else
        render :json => {:request_status => :failure}, :status => :ok
      end
    end
  end
end
